# TweetProc
TweetProc is an open source ETL pipeline system for processing tweets in real-time. TweetProc leverages Confluent Platform to extract Twitter data, translate it into French, and post the newly translated tweet on Twitter.

A custom built Kafka Source Connector ingests tweets from Twitter. Next, a custom built Kafka Streams application processes the tweets and performs the translation, and finally, a custom built Kafka Sink Connector pulls the processed tweets from the Kafka broker to post the tweet onto Twitter.

All components are coded and tested in Java.

## Architecture
![Semantic description of image](/images/tweetproc-design.png "TweetProc Design")

## To start using TweetProc


